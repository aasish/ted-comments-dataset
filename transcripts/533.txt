 What I want to do today is to spend some time talking about some stuff that's sort of given me a little bit of existential angst, for lack of a better word,  over the past couple of years.
 And basically these three quotes tell what's going on.
 When God made the color purple God was just showing off  Alice Walker wrote in The Color Purple.
 And Zora Neale Hurston in Dust Tracks on a Road, Research is a formalized curiosity.
 It's poking and prying with a purpose.
 And then finally, when I think about the near future, you know we have this attitude,  Well whatever happens, happens.  Right? So that goes along with the Cheshire Cat saying, If you don't care much where you want to get to, it doesn't much matter which way you go.
 But I think it does matter  which way we go, and what road we take because when I think about design in the near future, what I think are the most important issues, what's really crucial and vital, is that we need to revitalize the arts and sciences right now in 2002.
 (Applause) If we describe the near future as 10, 20, 15 years from now,  that means that what we do today is going to be critically important because in the year 2015, in the year 2020, 2025, the world our society is going to be building on, the basic knowledge and abstract ideas, the discoveries that we came up with today.
 Just as all these wonderful things we're hearing about here at the TED conference, that we take for granted in the world right now, were really knowledge and ideas that came up in the '50s, the '60s, and the '70s.
 That's the substrate that we're exploring today.
 Whether it's the internet, genetic engineering, laser scanners, guided missiles, fiber optics, high-definition television, sensing, remote-sensing from space, and the wonderful remote-sensing photos that we're seeing, 3D weaving, TV programs like Tracker, and Enterprise, CD read/write drives, flat screen, Alvin Ailey's Suite Otis or Sarah Jones' Your Revolution Will Not Be Between These Thighs, which by the way is banned by the FCC.
 Or ska, all of these things without question, almost without exception, are really based on ideas and abstract, and creativity from years before.
 So we have to ask ourselves, What are we contributing to that legacy right now? And when I think about it, I'm really worried.
 To be quite frank I'm concerned.
 I'm skeptical that we're doing very much of anything.
 We're in a sense, failing to act in the future.
 We are purposefully, consciously being laggards.
 We're lagging behind.
 Frantz Fannon who was a psychiatrist from Martinque said, Each generation must, out of relative obscurity, discover its mission, and fulfill or betray it.
 What is our mission?  What do we have to do? I think our mission is to reconcile to  reintegrate science and the arts because right now there's a schism that exists in popular culture.
 You know people have this idea that science and the arts are really separate.
 We think of them as separate and different things.
 And this idea was probably introduced centuries ago.
 But it's really becoming critical now because we're making decisions about our society every day, that if we keep thinking that the arts are separate from the sciences, and we keep thinking it's cute to say, I don't understand anything about this one.
 I don't understand anything about the other one. 
 Then we are going to have problems.
 Now I know no one here at TED thinks this.
 All of us, we already know that they're very connected.
 But I'm going to let you know that some folks in the outside world, believe it or not, they think it's neat when they say you know, Scientists and science is not creative.
 Maybe scientists are ingenious, but they're not creative.
 And then we have this tendency that career counselors, and various people say things like, Artists are not analytical.
 They're ingenious perhaps, but not analytical.
 And when these concepts underlie our teaching, and what we think about the world, then we have a problem because we stymie support for everything.
 By accepting this dichotomy, whether it's tongue in cheek, when we attempt to accommodate it in our world, and we try to build our foundation for the world, we're messing up the future because who wants to be uncreative? Who wants to be illogical? Talent would run from either of these fields, if you said you have to choose either.
 Then they're going to go to something where they think, Well I can be creative and logical at the same time.
 Now I grew up in the '60s.  And I'll admit it, actually my childhood spanned the '60s.
 And I was a wanna-be hippie.
 And I always resented the fact that I wasn't really old enough to be a hippie.
 I know there are people here, you know, the younger generation of wanna-be hippies.
 People talk about the '60s all the time and they talk about the anarchy that was there.
 But when I think about the '60s, what I took away from it was that there was hope for the future.
 We thought everyone could participate.
 There were wonderful, incredible ideas that were always percolating. 
 And so much of what's cool or hot today is really based on some of those concepts --  whether it's people trying to use a Prime Directive from Star Trek, being involved in things, or again that three-dimensional weaving, and fax machines that I read about in my weekly readers, as the technology and engineering was just getting started.
 But the '60s left me with a problem.
 You see, I always assumed I would go into space because I followed all of this.
 But I also loved the arts and sciences.
 You see, when I was growing up as a little girl and a teenager, I loved designing and making doll clothes, and wanted to be a fashion designer.
 I took art and ceramics.
 I loved dance --  Lola Falana, Alvin Ailey, Jerome Robins.
 And I also avidly followed the Gemini and the Apollo programs.
 I had science projects and tons of astronomy books.
 I took calculus and philosophy.
 I wondered about the infinity and the Big Bang theory.
 And when I was at Stanford, I found myself, my senior year -- chemical engineering major --  half the folks thought I was a political science and performing arts major, which was sort of true, because I was Black Student Union President, and I did major in some other things.
 And I found myself, the last quarter, juggling chemical engineering separation processes, logic classes, nuclear magnetic resonance spectroscopy, and also producing and choreographing a dance production.
 And I had to do the lighting and the design work.
 I was trying to figure out, do I go to New York City to try to become a professional dancer? Or do I go to medical school? Now my mother helped me figure that one out.
 (Laughter) But when I went into space, when I went into space I carried a number of things up with me.
 I carried a poster by Alvin Ailey -- who you can figure out now I love the dance company -- an Alvin Ailey poster of Judith Jameson performing the dance Cry, dedicated to all black women everywhere, a Bundu statue, which was from the Women's Society in Sierra Leone, and a certificate for the Chicago public school students to work to improve their science and math.
 Folks ask me, Why did you take up what you took up? I had to say, Because it represents human creativity.
 The creativity that allowed us, that we were required to have to conceive and build and launch the space shuttle, springs from the same source of imagination and analysis that it took to carve a Bundu statue, or the ingenuity it took  to design, choreograph, and stage Cry.
 Each one of them are different manifestations, incarnations of creativity.
 Avatars of human creativity.
 And that's what we have to reconcile in our minds, how these things fit together.
 The difference between arts and sciences is not analytical versus intuitive.  Right? E=mc² required an intuitive leap.
 And then you had to do the analysis afterwards.
 Einstein said in fact, The most beautiful thing we can experience is the mysterious.
 It is the source of all true art and science.
 Dance requires us to express, and want to express, the jubilation in life.
 But then you have to figure out, Exactly what movement do I do to make sure that it comes across correctly? The difference between arts and sciences is also not constructive versus deconstructive.  Right? A lot people think of the sciences as deconstructive.
 You have to pull things apart.
 Yes subatomic physics is deconstructive.
 You literally try to tear atoms apart  to understand what's inside of them.
 But sculpture, from what I understand from great sculptors, is deconstructive because you see a piece, and you remove what doesn't need to be there.
 Biotechnology is constructive.
 Orchestral arranging is constructive.
 So in fact we use constructive and deconstructive techniques in everything.
 The difference between science and the arts is not that they are different sides of the same coin even, or even different parts of the same continuum, but rather, they are manifestations of the same thing.
 The arts and sciences are avatars of human creativity.
 It's our attempt as humans to build an understanding of the universe, the world around us.
 It's our attempt to influence things --  the universe internal to ourselves and external to us.
 The sciences, to me, are manifestations of our attempt to express or share our understanding, our experience,  to influence the universe external to ourselves.
 It doesn't rely on us as individuals.
 It's a universe as experience by everyone.
 The arts manifest our desire, our attempt to share or influence others, through experiences that are peculiar to us as individuals.
 Let me say it again another way.
 Science provides an understanding of a universal experience.
 Arts provides a universal understanding of a personal experience.
 That's what we have to think about --  that they are all part of us. They're all part of a continuum.
 It's not just the tools.  It's not just the sciences.
 You know, the mathematics and the numerical stuff and the statistics because we heard, very much on this stage, people talked about music being mathematical. Right.
 Arts don't just use clay, are not the only ones who use clay, light, and sound and movement.
 They use analysis as well.
 So people might say, Well, I still like that intuitive versus analytical thing because everybody wants to do the right brain, left brain thing.  Right? We've all been accused of being right brain or left brain at some point in time, depending on who we disagreed with.
 (Laughter) When people say intuitive, that's like you're in touch with nature, in touch with yourself and relationships.
 Analytical, you put your mind to work.
 And I'm going to tell you a little secret.  You all know this though.
 But sometimes people use this analysis idea that things are outside of ourselves, to say that this is what we're going to elevate as the true, most important sciences.  Right? And then you have artists.  And you all know this is true as well.
 Artists will say things about scientists because they say they are too concrete.
 They are disconnected with the world.
 But we've even had that here on stage.
 So don't act like that you all don't know what I'm talking about.
 We had the -- We had folks talking about the flat earth society and flower arrangers.
 So there is this whole dichotomy that we continue to carry along even when we know better.
 And folks say we need to chose either or.
 But it would really be foolish to chose either one.  
 Right?  Intuitive versus analytical, that's a foolish choice.
 That's foolish just like trying to choose between being realistic or idealistic.
 You need both in life.
 Why do people do this? I'm just going to quote a molecular biologist, Sydney Brenner, who is 70 years old so he can say this.
 He said, It's always important to distinguish between chastity and impotence.
 (Laughter) I want to share with you -- I want to share with you a little equation.  Okay.
 How do understanding science and the arts fit in to our lives, and what's going on, and the things that we're talking about here at the design conference? And this is a little thing I came up with.
 Understanding, and our resources, and our will, cause us to have outcomes.
 Our understanding is our science, our arts, our religion, how we see the universe around us, our resources, our money, our labor, our minerals, those things that are out there in the world that we have to work with.
 But more importantly there's our will.
 This is our vision, our aspirations of the future, our hopes, our dreams, our struggles and our fears, our successes, and our failures influence what we do with all of those.
 And to me, design and engineering, craftsmanship and skilled labor, are all the things that work on this, to have our outcome, which is our human quality of life.
 Let me finish by saying, that my personal design issue in the future  is really about integrating to think about that intuitive and that analytical.
 The arts and sciences are not separate.
 High school -- physics lesson before we leave.
 High school physics teacher used to hold up a ball.
 She would say, This ball has potential energy.
 But nothing will happen to it. It can't do any work until I drop it and it changes states.
 I like to think of ideas as potential energy. They're really wonderful. 
 But nothing will happen until we risk putting them into action.
 This conference is filled with wonderful ideas.
 We're going to share lots of things with people.
 But nothing is going to happen until we risk putting those ideas into action.
 We need to revitalize the arts and sciences today.
 We need to take responsibility for the future.
 We can't hide behind saying it's just for company profits, or it's just a business,  or I'm an artist, or an academician.
 Here's how you judge what you're doing.
 I talked about that balance between intuitive, analytical.
 Fran Lebowitz my favorite cynic, she said, The three questions of greatest concern -- now I'm going to add on to design -- is, is it attractive? That's the intuitive.
 Is it amusing?  The analytical.
 And does it know its place? The balance. 
 Thank you very much.
 (Applause)