###########################################################################
#                      Language Technologies Institute
#                        Carnegie Mellon University
#                           Copyright (c) 2012
#                           All rights reserved
#
#
#  This work is licensed under a Creative Commons Attribution 3.0
#  Unported License. More information can be obtained on
#  http://creativecommons.org/licenses/by/3.0/
#
#  You are free:
#   - to share -- to copy, distribute and transmit the work
#   - to adapt the work
#   - to make commercial use of this work
#
#  Under the following conditions:
#   - The work must retain this license information and
#     the disclaimer below.
#   - Any modifications must be clearly marked as such
#   - The original contributor's names should not be deleted
#   - The original author's names must not be used to endorse
#     or promote products derived from this work without specific
#     prior written permission.
#
#   With the understanding that:
#    - Any of the above conditions can be waived if you get
#      permission from the copyright holder.
#    - Where the work or any of its elements is in the public domain under
#      applicable law, that status is in no way affected by the license.
#    - In no way are any of the following rights affected by the license:
#       + Your fair dealing or fair use rights, or other applicable
#         copyright exceptions and limitations
#       + The author's moral rights
#       + Rights other persons may have either in the work itself or
#         in how it is used, such as publicity or privacy rights.
#
#  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK
#  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS WORK, INCLUDING
#  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT
#  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE
#  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
#  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
#  ARISING OUT OF OR IN CONNECTION WITH THE USE OF THIS DATA.
#
###########################################################################
#  Author: Aasish Pappu (aasish@cs.cmu.edu)
#   Date : September 2012
###########################################################################
#  TED Comments Dataset
#
#  This data was retrieved by crawling the TED website. 
###########################################################################

=====================
TED comments and transcripts dataset
=====================
:Info: TED talks are freely available talks on the web www.ted.com on various disciplines of knowledge/wisdom.
:Author: Aasish Pappu and Gopala Krishna Anumanchipalli

Contact: aasish@cs.cmu.edu

Introduction
=====================
This dataset is based on TED which is a multi-theme conference
with speeches from distinguished individuals in fields of Culture,
Science, Climate etc. The videos of the talks are hosted at
http://www.ted.com free to watch and comment on for registered
users. Though TED has been around since the 90's, the activity has
risen only in recent times, since April 2007.

The nature of the talks is diverse and cater broad range of audience,
but there is a natural power law, in terms of popularity of a
particular class of talks.  For instance, we found that there are more
than 180 talks about Culture, and just one talk tagged under Success.

The dataset contains 667 talks with 19752 comments from active
users. We have filtered active users if they have written at least 5
comments on TED. Otherwise, we found that there are more than 555,000
registered users on TED, but only 2508 are most active among the lot.


Organization of Dataset
=====================

** all_authors.txt: contains names of the speakers in this dataset

** all_users.txt: contains the ids of the commenters in this dataset

** Talks_hyperlinks_with_titles.txt: contains the talk titles and the links to the actual talks on the web

** talks_themes.txt: TED talks are thematic e.g., Culture, Education, Science etc.

** talks_tags.txt: TED talks are "hash-tagged" with various labels. 

** TEDTalks.tsv: a tab-separated file with the URL, ID, URL, Speaker Name, Short Summary, Event, Duration, Publish date.

** talk_id.txt: contains just the talk and its id. 

** ap_gka_ted_comments_report.pdf: Report describing the experiments performed using this data set. 


Analysis and Transcripts
=====================

*** images:  Contains analysis of communities in the TED network and automatically generatedclusters of talks and users based on the comments language.  Read the report for more details. 

*** transcripts: contains the original transcripts of the TED talks (courtesy: Jao Miranda, LTI-Portugal program Ph.D. student)

Since the audio files are huge, can be provided on request (send email to aasish@cs.cmu.edu).
